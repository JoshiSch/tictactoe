package Projekte

import spock.lang.Specification


class TicTacToeTest extends Specification {


    def "evaluate"() {
        String[][] spielfeldA =        [['O', 'X', 'X'],
                                        ['X', 'O', 'X'],
                                        ['O', 'O', 'O']]

        /*String[][] spielfeldB =      [['X', 'X', 'X'],
                                      ['O', 'X', 'O'],
                                      ['O', 'O', 'X']]*/

        expect:
        if(TicTacToe.evaluate(spielfeldA) == "X - WINNER"){

            System.out.println("X is the Winner!")
        }

        if(TicTacToe.evaluate(spielfeldA) == "O - WINNER"){

            System.out.println("O is the Winner!")
        }

        if(TicTacToe.evaluate(spielfeldA) == "draw"){
            System.out.println("DRAW!")
        }

        if(TicTacToe.evaluate(spielfeldA) == "undecided" && TicTacToe.evaluate(spielfeldA) != "O - WINNER" && TicTacToe.evaluate(spielfeldA) != "X - WINNER"){
            System.out.println("undecided!")
        }




        //TicTacToe.evaluate(spielfeldB) == ""
        /*
        if(mode=="") {
            System.out.println("Ein Fehler ist aufgetreten.");
        }
        else{
                    System.out.println(mode);
        }
        */

}




    def"isValid"(){


        //First
        String[][] spielfeldA =        [['X', 'O', 'X'],
                                        ['O', 'O', 'O'],
                                        ['X', 'X', 'X']]
        //Second
        String[][] spielfeldB =      [['', '', ''],
                                      ['', '', ''],
                                      ['', '', '']]

        expect:

        //if invalid
        if(TicTacToe.isValid(spielfeldA)==false)
        {

            System.out.println("\n************************************************")
            System.out.println("Invalid move! Try again.")

            System.out.println("************************************************")
        }

        //If valid
        if(TicTacToe.isValid(spielfeldA)==true) {

            System.out.println("\n-------------")
            for(int i =0; i<=2; i++)
            {
                for (int n = 0; n <= 2; n++) {
                    spielfeldB[i][n] = spielfeldA[i][n]
                    System.out.print("| " + spielfeldB[i][n] + " ")
                }
                System.out.print("|\n-------------\n")
            }
        }

    }

}
