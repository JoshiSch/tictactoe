/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tictactoe;

/**
 *
 * @author joshi
 */
public class TicTacToe {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String[][] spielfeld = {{"", "X", ""},
                                {"O", "O", "O"},
                                {"O", "X", "X"}};
        
        
        String test = evaluate(spielfeld);
        System.out.println(test);
        
    }
    
    
    public static String evaluate(String[][] sf){
        
        int winner = 0;
        
        for(int i=0;i<3;i++){
            if(sf[i][0].equals("X") && sf[i][1].equals("X") && sf[i][2].equals("X")){
                winner = 1;
            }
            if(sf[0][i].equals("X") && sf[1][i].equals("X") && sf[2][i].equals("X")){
                winner = 1;
            }
            if(sf[i][0].equals("O") && sf[i][1].equals("O") && sf[i][2].equals("O")){
                winner = 2;
            }
            if(sf[0][i].equals("O") && sf[1][i].equals("O") && sf[2][i].equals("O")){
                winner = 2;
            }   
        }
        
        if(sf[0][0].equals("X") && sf[1][1].equals("X") && sf[2][2].equals("X")){
                winner = 1;
        }
        if(sf[0][2].equals("X") && sf[1][1].equals("X") && sf[2][0].equals("X")){
                winner = 1;
        }
        if(sf[0][0].equals("O") && sf[1][1].equals("O") && sf[2][2].equals("O")){
                winner = 2;
        }
        if(sf[0][2].equals("O") && sf[1][1].equals("O") && sf[2][0].equals("O")){
                winner = 2;
        }
        
        
        int count = 0;
        for(int o=0;o<3;o++){
            for(int p=0;p<3;p++){
                if(sf[o][p].equals("X") || sf[o][p].equals("O")){
                    count++;
                }
            }
        }
        
        if(count>=7 && winner == 0){
            return "draw";
        }
        if(winner == 1){
            return "X";
        }
        if(winner == 2){
            return "O";
        }
        if(count<7){
            return "undecided";
        }
        return "Fehler";
        
    }
    
    
    public static boolean isValid(String[][] sf){
        
        int winner = 0;
        
        for(int i=0;i<3;i++){
            if(sf[i][0].equals("X") && sf[i][1].equals("X") && sf[i][2].equals("X")){
                winner = 1;
            }
            if(sf[0][i].equals("X") && sf[1][i].equals("X") && sf[2][i].equals("X")){
                winner = 1;
            }
            if(sf[i][0].equals("O") && sf[i][1].equals("O") && sf[i][2].equals("O")){
                winner = 1;
            }
            if(sf[0][i].equals("O") && sf[1][i].equals("O") && sf[2][i].equals("O")){
                winner = 1;
            }   
        }
        
        if(sf[0][0].equals("X") && sf[1][1].equals("X") && sf[2][2].equals("X")){
                winner = 1;
        }
        if(sf[0][2].equals("X") && sf[1][1].equals("X") && sf[2][0].equals("X")){
                winner = 1;
        }
        if(sf[0][0].equals("O") && sf[1][1].equals("O") && sf[2][2].equals("O")){
                winner = 1;
        }
        if(sf[0][2].equals("O") && sf[1][1].equals("O") && sf[2][0].equals("O")){
                winner = 1;
        }
        
        
        int count = 0;
        for(int o=0;o<3;o++){
            for(int p=0;p<3;p++){
                if(sf[o][p].equals("X") || sf[o][p].equals("O")){
                    count++;
                }
            }
        }
        
        if(count>=7 && winner == 0){
            return false;
        }else{
            return true;
        }
        
    }
    
}
