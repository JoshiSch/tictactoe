package Projekte;

import java.util.Scanner;

public class TicTacToe {


    public static String evaluate(String[][] spielfeldA) {

        String mode="draw";

        for (int i = 0; i <= 2; i++) {
            for (int n = 0; n <= 2; n++) {

                if (spielfeldA[i][n].equals("")) {
                    mode = "undecided";
                }

                //oben nach unten geprüft
                if (spielfeldA[0][0].equals("X") && spielfeldA[1][0].equals("X") && spielfeldA[2][0].equals("X") || spielfeldA[0][1].equals("X") && spielfeldA[1][1].equals("X") && spielfeldA[2][1].equals("X") || spielfeldA[0][2].equals("X") && spielfeldA[1][2].equals("X") && spielfeldA[2][2].equals("X")){

                    mode= "X - WINNER";
                }

                if (spielfeldA[0][0].equals("O") && spielfeldA[1][0].equals("O") && spielfeldA[2][0].equals("O") || spielfeldA[0][1].equals("O") && spielfeldA[1][1].equals("O") && spielfeldA[2][1].equals("O") || spielfeldA[0][2].equals("O") && spielfeldA[1][2].equals("O") && spielfeldA[2][2].equals("O")){

                    mode= "O - WINNER";
                }

                //links nach rechts geprüft ---X
                if(spielfeldA[0][0].equals("X") && spielfeldA[0][1].equals("X") && spielfeldA[0][2].equals("X") || spielfeldA[1][0].equals("X") && spielfeldA[1][1].equals("X") && spielfeldA[1][2].equals("X") || spielfeldA[2][0].equals("X") && spielfeldA[2][1].equals("X") && spielfeldA[2][2].equals("X")) {

                    mode ="X - WINNER";
                }

                //links nach rechts geprüft ---O
                if(spielfeldA[0][0].equals("O") && spielfeldA[0][1].equals("O") && spielfeldA[0][2].equals("O") || spielfeldA[1][0].equals("O") && spielfeldA[1][1].equals("O") && spielfeldA[1][2].equals("O") || spielfeldA[2][0].equals("O") && spielfeldA[2][1].equals("O") && spielfeldA[2][2].equals("O")) {

                    mode ="O - WINNER";
                }


                //Quer überprüfen ---X
                if(spielfeldA[0][0].equals("X") && spielfeldA[1][1].equals("X") && spielfeldA[2][2].equals("X") || spielfeldA[0][2].equals("X") && spielfeldA[1][1].equals("X") && spielfeldA[2][0].equals("X")) {

                    mode ="X - WINNER";
                }

                //Quer überprüfen ---O
                if(spielfeldA[0][0].equals("O") && spielfeldA[1][1].equals("O") && spielfeldA[2][2].equals("O") || spielfeldA[0][2].equals("O") && spielfeldA[1][1].equals("O") && spielfeldA[2][0].equals("O")) {

                    mode ="O - WINNER";
                }

                }
            }


        return mode;
    }

    public static boolean isValid(String[][] spielfeldA) {


        int countX = 0;
        int countO = 0;
        int countOpen = 0;


        for (int i = 0; i <= 2; i++) {
            for (int n = 0; n <= 2; n++) {
                if (spielfeldA[i][n].equals("X")) {
                    countX++;
                }

                if (spielfeldA[i][n].equals("O")) {
                    countO++;
                }

            }

        }


        countOpen = 9 - (countO + countX);

        if (countX - countO > 1 || countO - countX > 1) {


            //System.out.println("Open-Felder: "+countOpen+"\n O-Felder: "+countO+"\n X-Felder: "+countX);

            return false;

        } else {
            return true;
        }


    }
}


